import React from 'react';
import ReactDOM from 'react-dom';
import 'isomorphic-fetch';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App.jsx';

Enzyme.configure({ adapter: new Adapter() });

// Test if the <App /> works
it('App renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

// Test name sort function
test('App.sortByName() sorts array', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.instance().sortByName({id:1,name:'Alana'}, {id:2,name:'Aaron'})).toBe(1);
})

// Test onMenuLinkClick() change the state
test('App.onMenuLinkClick() sets state', () => {
	const div = document.createElement('div');
	const wrapper = shallow(<App />);

	div.addEventListener('click', (e) => {
		wrapper.instance().onMenuLinkClick({id:0, name:'Fluke'}, e, true);
	})

	div.click();

	expect(wrapper.instance().state.selectedPets).toMatchObject([{ id: 0, name: 'Fluke' }]);
})
