import React from 'react';
import {Link} from '../components/Menu.jsx';
import renderer from 'react-test-renderer';

test('Link changes the class when clicked', () => {
  const component = renderer.create(
    <Link key="0" name={{id:0, name:'Fluke'}} onClick={(e) => console.log('clicked')}>Fluke</Link>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // manually trigger the callback
  tree.props.onClick();
  // re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});