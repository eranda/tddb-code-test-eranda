import React from 'react';
import Menu from './components/Menu.jsx';
import '../scss/main.scss';

/**
 * The main component that renders the entire app.
 */
export default class App extends React.Component {
  constructor (props) {
    // gain access to super class
    super();
    // set initial state
    this.state = {
      sortedPets: [],
      selectedPets: []
    };
    // bind 'this' context to methods
    this.loadData = this.loadData.bind(this);
    this.prepData = this.prepData.bind(this);
    this.onMenuLinkClick = this.onMenuLinkClick.bind(this);
  }

  /**
  * Prepare the loaded data in a output friendly way.
  *
  * @param {object} data - Data to be sorted (as Pet type > Owner gender > Pet's name)
  * @public
  */
  prepData (data) {
    // A temp array to store the new structure of data
    let sortedPets = [];
    // Loop through each owner
    data.forEach((owner, ownerIndex) => {
      // Loop through each pet of the current owner
      owner.pets && owner.pets.forEach((pet, petIndex) => {
        // Does the current pet type exists? If not, create it and assign an empty array
        !sortedPets[pet.type] && (sortedPets[pet.type] = []);
        // Does the current pet type > gender exists? If not, create it and assign an empty array
        !sortedPets[pet.type][owner.gender] && (sortedPets[pet.type][owner.gender] = []);
        // Add the pet object name under type > gender
        // We need unique ID here to identify the pet.
        sortedPets[pet.type][owner.gender].push({
          'id': `${ownerIndex}${owner.gender}${petIndex}`,
          'name': pet.name
        });
      });
    })

    // Sort pets by name
    Object.keys(sortedPets).map((petType) => {
      Object.keys(sortedPets[petType]).map((ownerGender) => {
        sortedPets[petType][ownerGender].sort(this.sortByName);
      })
    });

    // Assign the sorted array to the state
    this.setState({
      sortedPets: sortedPets
    });
  }

  /**
  * Load the json form a given URL.
  *
  * @param {string} url
  * @param {function(object)} callback - A callback to prepare data
  * @public
  */
  loadData (url, fnSuccess) {
    // Use fetch API to load json
    window.fetch(url)
      .then((response) => {
        if (response.ok) {
          // fetch operatoin successfull. Convert json string to JS object and return
          return response.json();
        }
        // Response is not right. Throw an error.
        throw new Error('Network response was not ok.');
      })
      .then((data) => {
        // Call the success callback
        fnSuccess(data);
      })
      .catch((error) => {
        // There is problem. Throw an error.
        throw new Error('There has been a problem with fetch operation: ' + error.message);
      });
  }

  onMenuLinkClick (pet, event, selected) {
    event.preventDefault();
    // Get the selectedPets so we can manipulate it
    let selectedPets = this.state.selectedPets;

    if (selected) {
      // Add the pet object to 'selectedPets'
      selectedPets.push(pet);
    } else {
      // Remove the pet object from 'selectedPets'
      selectedPets.splice(selectedPets.indexOf(pet), 1);
    }

    // Sort by pet's name
    selectedPets.sort(this.sortByName);
    // Set the 'selectedPets' state
    this.setState({
      selectedPets: selectedPets
    });
  }

  sortByName (a, b) {
    // Compere the string as a number for ordering
    return a.name.localeCompare(b.name);
  }

  /**
  * React componentDidMount lifecycle hook.
  *
  * @public
  */
  componentDidMount () {
    // Component has been mounted. Let's load the json
    this.loadData('http://test.stage.tribalddbmelb.com.au/people.json', this.prepData);
  }

  /**
  * React render lifecycle hook.
  *
  * @public
  */
  render () {
    return (
      <div className="row">
        <div className="col half">
          <Menu sortedPets={this.state.sortedPets} onClick={this.onMenuLinkClick} />
        </div>
        <article className="col half">
          <strong>You selected:</strong> {this.state.selectedPets.map((pet, index) => {
            if (this.state.selectedPets.length === (index + 1)) {
              return pet.name;
            }
            return `${pet.name}, `;
          })}
        </article>
      </div>
    );
  }
}
