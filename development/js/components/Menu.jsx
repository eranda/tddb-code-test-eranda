import React from 'react';

/**
 * The <Menu /> react component responsible to generate the entire menu
 */
export class Menu extends React.Component {
  constructor (props) {
    // gain access to super class
    super(props);
    // bind methods to 'this' context
    this.generateMenu = this.generateMenu.bind(this);
  }

  /**
  * Prepare the loaded data in a output friendly way.
  *
  * @param {object} items - Items to generate the menu from
  * @param {int} [depth=0] - Depth of currnt call
  * @public
  */
  generateMenu (items, call = 1) {
    // Objects are not easy to iterate. Lets the keys array
    const names = Object.keys(items);

    // Keep track of currnt depth
    const depth = call % 3;

    // Return jsx
    return (
      // Wraper <ul> with key
      <ul key={depth} className="menu__items">
        {
          // Loop through all the object keys
          names.map((name, nameIndex) => {
            // Prepare output
            let output = null;
            if (Array.isArray(items[name])) {
              // If the current element is an array, output <a> and child <ul>
              output = [
                <span key={depth} className={`menu__link menu__link--${depth}`}>{name}</span>,
                // Need the sub items (<ul>..)
                this.generateMenu(items[name], (call + 1))
              ];
            } else {
              // End of the tree, just output <Link>. No more sibling <ul>
              output = <Link key={items[name].id} name={items[name]} onClick={this.props.onClick}>{items[name].name}</Link>;
            }
            // Output the current <li>
            return <li key={nameIndex} className="menu__item">{output}</li>;
          })
        }
      </ul>
    );
  }

  /**
  * React render lifecycle hook.
  *
  * @public
  */
  render () {
    return <nav className="menu">{this.generateMenu(this.props.sortedPets)}</nav>;
  }
}

/**
 * The <Link /> react component responsible to generate a single link
 */
export class Link extends React.Component {
  constructor (props) {
    // gain access to super class
    super(props);

    // set initial state
    this.state = {
      selected: false
    };

    // bind methods to 'this' context
    this.onClick = this.onClick.bind(this);
  }

  /**
  * Link on click handler.
  *
  * @param {object} event - Event object
  * @public
  */
  onClick (event) {
    // Call the onClick from props
    this.props.onClick(this.props.name, event, !this.state.selected)
    // Toggle the selected state
    this.setState({
      selected: !this.state.selected
    });
  }

  /**
  * React render lifecycle hook.
  *
  * @public
  */
  render () {
    // Toggle selected class
    const selectedClass = this.state.selected ? 'menu__link--3-selected' : '';
    return <a href="#" onClick={this.onClick} className={`menu__link menu__link--3 ${selectedClass}`}>{this.props.children}</a>
  }
}

export default Menu;
