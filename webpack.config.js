"use strict";
var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var autoprefixer = require('autoprefixer');

// dev server host and port
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || '8008';

module.exports = {
  // entry to the compile JS
  // 'react-hot-loader/patch' is required for HMR
  entry: [
    'react-hot-loader/patch',
    './development/js/index.jsx',
  ],
  // we want source maps on dev builds
  devtool: 'cheap-module-source-map',
  // settings related to the output of the compilation
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist'),
    filename: 'assets/js/main.js'
  },
  // change webpack default resolve
  resolve: {
    extensions: ['.js', '.jsx']
  },
  // configure loaders for different file types
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: ['node_modules'],
        loader: 'babel-loader',
        query:{
          presets:['react','es2015']
        }
      },
      // for SCSS, 'ExtractTextPlugin' has been used to generate a
      // seperate CSS file in dev, eady soucemaps and and better CSS
      // management for production
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader?sourceMap', 
            {
              loader: 'postcss-loader',
              options: {
                plugins: function () {
                  //browserslist in package.json
                  return [autoprefixer]
                },
                sourceMap:true
              }
            },
            'sass-loader?sourceMap'
          ]
        })
      }
    ]
  },
  // dev server and HMR configuration
  devServer: {
    contentBase: './dist',
    noInfo: true,
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: PORT,
    host: HOST
  },
  // define and configure plugins
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: 'assets/css/style.css',
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: './development/views/template.html',
      files: {
        css: ['style.css'],
        js: [ 'main.js'],
      }
    })
  ]
};
