# Tribal DDB Front End Developer Test #

This repository contains solution I built for Tribal DDB Front End Developer test as per the requirements of http://test.stage.tribalddbmelb.com.au/

## Getting started ##

### How to run ###

Clone the repo:
```
git clone https://eranda@bitbucket.org/eranda/tddb-code-test-eranda.git
```

CD to the project folder:
```
cd tddb-code-test-eranda
```

Install node dependencies:
```
npm install
```

Run the project:
```
npm run start
```

The above command should open a new browser window pointing to http://127.0.0.1:8008/ and load the dev site. Visit http://127.0.0.1:8008/ if the browser does not open automatically.

### Production and sample ###

Generate a production version:
```
NODE_ENV='production' webpack -p
```

A sample production version generated with the above command can be found at http://tddb-code-test-eranda.s3-website-ap-southeast-2.amazonaws.com/

### Testing ###

Test application:
```
npm test
```


## Notes ##

### Setup ###
The workflow used here is a boilerplate I use on my projects and evolves  all the time. It is inspired by https://github.com/alicoding/react-webpack-babel. I have only used some mixins from bootstrap to keep styles simple and lean. Additionally  I did not use a third party JS library like Lodash or jQuery since everything I wanted was achievable with native features 

### Assumptions ###

The brief had few requirements that made me assume the following
 - *select a list of all the pets names* - The reason to make the pet names selectable
 - *by type* - The reason to have the  top level (Cat, Dog, Fish)
 - *in alphabetical order* - The reason to order the pet names in the menu and in the RHS
 

### Challenges and lessons learned ###
- Arrays behave same as objects when the keys are not numeric 
- Did fair amount of work to get Jest working

### Need to contact me? ###

If you are having any trouble running the project or need to discuss further, please contact me on erandahettiarachchi@gmail.com
